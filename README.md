Dépot git de mon mémoire de thèse.
==================================

La manière la plus simple de compiler le manuscrit passe par `nix-env` (il
faut avoir le gestionnaire de paquet `nix` installé [0]). Cet outil se charge de
récupérer ou construire toutes les dépendances nécessaires à la compilation,
puis de les mettre à disposition dans l'environnement de l'utilisateur.

Dans le répertoire racine du dépot, entrez:

    $ nix-shell

puis (à cause du fonctionnement de tex):

    $ buildthesis && buildthesis

Le PDF est également disponible [directement](https://framagit.org/mpo/doctorat-manuscrit/blob/master/preview.pdf).

LACL - Université Paris Est Créteil
Financé par le projet ANR SYNBIOTIC
Martin POTIER

[0] http://nixos.org/nix/
