with import <nixpkgs> {};

# My thesis-specific tools and utilities
stdenv.mkDerivation {
  name = "thesis-bundle";
  buildInputs = [
    ( texlive.combine {
      inherit (texlive)
      scheme-basic
      collection-langfrench
      algorithm2e
      biblatex
      caption
      enumitem
      euenc
      filehook
      jknapltx
      listings
      logreq
      metafont
      minitoc
      ms
      multirow
      pgf
      pgfplots
      placeins
      polyglossia
      relsize
      rsfs
      setspace
      siunitx
      standalone
      ucharcat
      unicode-math
      xcolor
      xetex
      xetex-def
      xkeyval
      xstring
      zapfding
      ;} )
      biber
      (import ./builderbot {})
    ];
    src=null;
    shellHook = ''
      mkdir -p /tmp/build-thesis
      echo "Juste type 'buildthesis' to build the thesis"
    '';
  }
