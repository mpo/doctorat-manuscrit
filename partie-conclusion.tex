\chapter{Conclusion}

Dans ce très court chapitre, nous résumons les chapitres précédents et
présentons nos contributions au projet de recherche ANR \synbiotic.

\section{Résumé de nos travaux}

\paragraph{\nameref{chap:partie-multi-modele}} Dans ce chapitre, nous
présentons la définition d'un formalisme unifiant dans le but de permettre
la spécification commune et la classification de modèles. En partant du
constat que modéliser revenait à définir une loi d'exclusion, c'est à
dire distinguer les comportements acceptés des comportements rejetés
par le modèle, nous avons donné une définition d'un modèle comme un
couple $(\sig,\bhv)$ constitué de la signature du modèle \sig{} et de son
comportement \bhv{}. Un modèle peut être construit suivant différentes
méthodes: par extension, dans le cas d'un modèle expérimental par exemple,
ou bien par intention. Après avoir fourni quelques exemples de reformulation
de modèles classiques dans notre formalisme, nous nous sommes attaqués à
l'expression des relations pouvant exister entre différents modèles. Il est
apparu nécessaire de considérer un modèle de référence pour définir
une flèche d'abstraction entre deux modèles : le modèle de référence
se présente comme le lien au système étudié, et peut être un modèle
expérimental. Nous avons également étudié l'expression formelle d'une
composition entre deux modèles. Ces définitions nous ont permis d'établir une
construction cohérente constituée de plusieurs modèles en relation les uns
avec les autres fournissant aussi une première partie concrète de réponse
au problème de la construction de modèles constitués de sous-modèles en
relation les uns avec les autres.

\paragraph{\nameref{chap:partie-activite}} Dans ce chapitre, nous abordons une
nouvelle technique de modélisation fondée sur l'activité spatiale dans le cadre
d'un langage informatique pour la modélisation et la simulation spatiale nommé
\MGS. \MGS{} est un langage de programmation non-conventionnel qui met l'accent
sur la place centrale de l'espace dans la modélisation. Spécifier un modèle avec
\MGS{} revient à choisir une structure de donnée adaptée, appelée collection
topologique, et une fonction de transition, la transformation. Une simulation
d'un modèle écrit avec \MGS{} correspond à la réécriture successive de la
collection topologique choisie. Grâce à \MGS, nous avons introduit et mis en
situation un nouvel algorithme de calcul d'un front d'activité: premièrement cet
algorithme généralise des optimisations déjà connues sur les automates
cellulaires à toute collection topologique, deuxièmement, notre algorithme rend
accessible comme donnée de premier ordre les zones spatialement actives pendant
la simulation d'un modèle écrit avec \MGS.

\paragraph{\nameref{chap:partie-otb}} Dans ce chapitre, nous présentons \otb, un
simulateur du comportement d'une population de bactéries \Ecoli{} écrit en OCaml
et en C. Ce simulateur repose sur un modèle du comportement des bactéries
\ecoli{} dans leur environnement qui est le fruit de la composition de trois
modèles:
\begin{itemize}
\item le \emph{moteur physique}, dédié au comportement physique des bactéries;
\item le \emph{moteur chimique}, dédié à la réaction et à la diffusion des
  morphogènes dans l'environnement des bactéries, et
\item le \emph{moteur de décision}, dédié à l'interaction de ces deux
  environnements, décrivant les interactions mutuelles entre les deux moteurs
  précédents, il régit le comportement de chaque bactérie et fait appel aux deux
  modèles précédents.
\end{itemize}
Les deux moteurs physiques et chimiques reposent sur les automates cellulaires
en deux dimensions pour lesquels nous avons développé une technique de
simulation originale, l'algorithme de Propagation Parallèle à la Margolus
(\ppm), dans le but de pouvoir simuler une population de l'ordre de \SI{E5}{}
individus sur des ordinateurs munis de cartes graphique grand public.

\section{Contribution à \synbiotic}

Ce manuscrit de thèse est un livrable du projet ANR \synbiotic.
Dans ce document nous avons contribué spécifiquement à deux
work-packages, WP2 (chapitre~\ref{chap:partie-activite}) et WP3
(chapitre~\ref{chap:partie-otb}), ainsi qu'au projet de recherche en général
(chapitre~\ref{chap:partie-multi-modele}):
\begin{description}
  \item[Contributions à WP2]
    %MGS colle parfaitement à L1 (interactions)
    Ce WP est dédié à l'élaboration d'un langage, nommé L1, de programmation
    spatiale dont la fonction est de décrire les exemples déterminés dans le
    WP1. Le langage \mgs, introduit dans le chapitre~\ref{chap:partie-activite},
    est un candidat idéal pour ce rôle.
    % Raison 1: Collection topologiques
    Tout d'abord, \mgs est construit sur les collections topologiques, une
    structure de donnée dans laquelle l'espace est traité explicitement.
    Il dispose d'une primitive spatiale s'appliquant à toute collection
    topologique: l'opérateur de voisinage «\ç{,}».
    %Reformulé avec les concepts du chapitre~\ref{chap:partie-multi-modele}, .
    Cet unique opérateur (et ses restrictions) permet de spécifier,
    indépendamment de la collection, le modèle de chaque exemple du WP1.
    % Raison 2: Transformations sur les collections
    Ensuite, dans \mgs, le temps est modélisé par les altérations successives
    d'une collection topologique au moyen d'une fonction définie par cas sur
    des motifs de filtrage d'une collection topologique appelée transformation.
    Cette fonction permet de traiter différents aspects temporels (asynchrone,
    synchrone, déterministe, stochastique, etc.) de l'évolution et se trouve
    particulièrement bien adapté à le description des exemples du WP1.
    % Raison 3: Activité est un outil pour la description des exemples du WP1
    Finalement, la mise en évidence de l'activité spatiale dans \mgs nous permet
    de simuler les exemples, dans certain cas, plus efficacement en restreignant
    le filtrage de motif à la partie active de la collection topologique, c'est
    à dire celle qui se trouvera modifiée au prochain pas de simulation.
  \item[Contributions à WP3]
    % OTB simule parfaitement L2
    Ce WP est dédié à l'élaboration d'un second langage, nommé L2, de
    programmation prenant en entrée les concepts utilisés en sortie de
    L1, et en les instanciant dans les individus (des bactéries). Le
    simulateur \otb, muni du langage de description \sbgp, introduit dans le
    chapitre~\ref{chap:partie-otb}, est un candidat idéal pour ce rôle.
    % Raison 1: Réseau de régulation génétique décrit par \sbgp
    Tout d'abord, le langage de description permettant d'instancier le
    comportement des bactéries est \sbgp. Il décrit l'équivalent d'un Réseau
    de Régulation Génétique (RRG) d'une bactérie, c'est à dire l'action de
    l'environnement sur l'expression de son code génétique. Dans \otb, ce RRG
    est décrit sous la forme d'un automate à états finis embarqué dans chacune
    des bactéries, ce qui permet de retrouver des comportements de population
    observés en laboratoire.
    % Raison 2: Simulation efficace
    Ensuite, nous avons fait en sorte que \otb soit le plus efficace possible,
    en utilisant le parallélisme à disposition dans les cartes graphiques
    grand public. Nous pouvons atteindre en quelques minutes une population de
    l'ordre des \num{E5} bactéries interagissant les unes avec les autres.
    % Raison 3: OTB vu comme une collection topologique de MGS
    Finalement, et même si cet objectif n'est pas encore atteint au moment
    de l'écriture de ce manuscrit, nous estimons que \otb peut-être vu comme
    une collection topologique particulière de \mgs, ce qui nous permettra
    de lier les langages L1 et L2 directement au niveau de \mgs. Ainsi, les
    primitives spatiales définies dans L1 pourront avoir une traduction
    explicite directement dans \mgs et servir à définir le RRG incorporé dans
    les bactéries de \otb.
  \item[Contributions générales]
    % Description du multi-niveau colle à tour de langages
    Le projet \synbiotic a pour objectif de décrire le passage d'un comportement
    de haut niveau, à l'échelle d'une population d'entités, à un comportement de
    bas niveau, à l'échelle de l'individu, par le passage le long d'une tour de
    langages, similaire au processus de compilation d'un programme informatique.
    % Raison 1: Chacun des étages de la tour forme un niveau de description
    Pour commencer, nous constatons que chacun de ces étages de cette tour de
    compilation peuvent être vu comme des niveau de modélisation. À chaque
    étage, un langage de programmation manipule des objets d'une certaine
    manière, avec une certaine préférence, par exemple, dans \mgs, l'accent
    est mis sur la description explicite de l'espace. Dans le cadre du
    chapitre~\ref{chap:partie-multi-modele}, nous pourrions voir cet étage comme
    un niveau de description où les modèles appartiennent tous à la classe des
    modèles à champ, en y ajoutant certainement quelques contraintes pour mieux
    coller à \mgs, les champs offrent une description plus abstraite que les
    opérateurs de \mgs nous permettent.
    % Raison 2: Nous avons un outil pour formaliser les liens entre chaque étage
    %           Est-ce un couplage simple / une complexification ?
    Ensuite, le cadre formel développé dans le
    chapitre~\ref{chap:partie-multi-modele}, nous donne un outil concret pour
    aller caractériser les classes de modèles spécifiques à chaque étages et à
    expliciter les liens entre chacun des étages de cette tour de langages. Nous
    sommes désormais en mesure, dans des travaux futurs, de déterminer quel type
    de flèche il existe entre chacun des niveaux de description du projet.
    De plus, la description de l'activité spatiale dans le
    chapitre~\ref{chap:partie-activite} nous donne déjà une voie vers une
    représentation intermédiaire entre L0, le langage de haut niveau pour la
    description des exemples du WP1 et L1.
    % Raison 3: Formalisation aide WP5 sur calculabilité et WP6 sur la
    %           specification de Bio-sûreté et bio-sécurité
    Finalement, les travaux effectués chapitre~\ref{chap:partie-multi-modele}
    devraient apporter un éclairage bienvenu sur les travaux des autres WP, en
    proposant un support à l'étude de la calculabilité des modèles de chacun des
    niveaux pour le WP5, et un cadre de description général permettant d'étudier
    au mieux le respect des spécifications dans le cadre du WP6.
\end{description}

% Clôture/Humour
Nos perspectives de recherche globales sont bien sûr plus nombreuses que la
somme des perspectives de recherche de chacune des parties. Est-ce bien le cas?
Nous le saurons bien assez tôt.

% ### THE END ###
