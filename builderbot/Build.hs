import System.IO
import Development.Shake
import Development.Shake.FilePath
import System.Directory (createDirectory)
import System.FilePath.Glob
import Data.List (isSuffixOf)

target :: FilePath
target = "main"

finalTarget :: FilePath
finalTarget = "potier.these.2015" <.> "pdf"

buildDir :: FilePath
buildDir = "_build"

fullTarget :: FilePath
fullTarget = buildDir </> target <.> "pdf"

sanityFile :: FilePath
sanityFile = buildDir </> "sanity.check"

texCmd :: String -> [String]
texCmd target = ["xelatex", "-halt-on-error", target]

foldersource :: FilePath -> [FilePattern] -> Action [FilePath]
foldersource folder wildcards = do
  files <- getDirectoryFiles folder wildcards
  return $ map (folder </>) files

foldersourceIO :: FilePath -> [String] -> IO [FilePath]
foldersourceIO folder wildcards = do
  let patterns = map compile wildcards
  (results,_) <- globDir patterns folder
  return $ concat results

allfiles :: Action [FilePath]
allfiles = do
  tex <- getDirectoryFiles "" ["*.tex"]
  bib <- foldersource "biblio" ["*"]
  dots <- foldersource "data" ["*"]
  figures <- foldersource "figures" ["*"]
  fonts <- foldersource "fonts" ["*"]
  let files = tex ++ bib ++ dots ++ figures ++ fonts
  return $ map (buildDir </>) files

-- Without link
compiledTikzFiguresIO :: IO [FilePath]
compiledTikzFiguresIO = do
  tikz <- foldersourceIO "figures" ["*.tikz"]
  let pdfs = map (-<.> "pdf") tikz
  return $ map (buildDir </>) $ filter (not . isSuffixOf "link.pdf") pdfs

main :: IO ()
main = do
  -- preping
  compiledTikzFigures <- compiledTikzFiguresIO

  shakeArgs shakeOptions { shakeFiles = buildDir
                         , shakeThreads = 0
                         , shakeProgress = progressSimple } $ do
    want [ finalTarget ]

    -- Populate when needed
    (map (buildDir </>) ["*.tex", "biblio/*", "data/*", "figures/*", "fonts/*"]) |%> \out -> do
      copyFile' (dropDirectory1 out) out

    -- creating a sanity file (erk!)
    sanityFile %> \out -> do
      writeFile' sanityFile "building sanely"

    -- Turn *.tikz in *.pdf
    compiledTikzFigures |%> \out -> do
      let source = out -<.> "tikz"
      need [source]
      cmd (EchoStdout False) [ "xelatex", "-halt-on-error",
                    "-output-directory=" ++ (buildDir </> "figures"), source]

    -- Build link.pdf
    buildDir </> "figures/link.pdf" %> \out -> do
      let source = out -<.> "tikz"
      need $ source : map (buildDir </>)
        [ "common-headers.tex", "sigles.tex",
          "figures/operateursS.pdf",  "figures/operateursStS.pdf",
          "figures/operateursfS.pdf", "figures/operateursfStS.pdf",
          "figures/operateursStfS.pdf", "figures/operateursLkS.pdf" ]

      cmd (Cwd buildDir) (EchoStdout False)
              [ "xelatex", "-halt-on-error",
                "-output-directory=figures", (dropDirectory1 source)]


    fullTarget %> \out -> do
      removeFilesAfter sanityFile ["*"]
      allSrc <- allfiles
      need $ sanityFile : (out -<.> "bbl")
                        : (buildDir </> "figures/link.pdf")
                        : compiledTikzFigures ++ allSrc
      cmd (Cwd buildDir) (EchoStdout False) $ texCmd target

    -- generate "main.bbl"
    fullTarget -<.> "bbl" %> \out -> do
      allSrc <- allfiles
      need $ (buildDir </> "figures/link.pdf") : compiledTikzFigures ++ allSrc

      existsAux <- doesFileExist $ fullTarget -<.> "aux"
      existsSane <- doesFileExist $ sanityFile
      if (not existsAux || existsSane)
        then cmd (Cwd buildDir) (EchoStdout False) $ texCmd target
        else return ()

      cmd (EchoStdout False) ["biber", dropExtension out]

    finalTarget %> \out -> do
      need [fullTarget]

      copyFileChanged fullTarget out

    phony "clean" $ do
      removeFilesAfter "_build" ["//*"]

