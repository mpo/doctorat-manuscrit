{ nixpkgs ? import <nixpkgs> {}, compiler ? "default" }:

let

  inherit (nixpkgs) pkgs;

  f = { mkDerivation, base, directory, Glob, shake, stdenv }:
  mkDerivation {
    pname = "buildthesis";
    version = "0.1.0.0";
    src = ./.;
    isLibrary = false;
    isExecutable = true;
    executableHaskellDepends = [ base directory Glob shake ];
    license = stdenv.lib.licenses.publicDomain;
    };

    haskellPackages = if compiler == "default"
    then pkgs.haskellPackages
    else pkgs.haskell.packages.${compiler};

  in
  haskellPackages.callPackage f {}

