\documentclass[crop,tikz]{standalone}
\input{common-headers}

\begin{document}

\tikzset{cell color/.style={black!20}}

\newcommand{\common}{%
  \coordinate (ll) at (-2.5cm,-2.5cm);
  \coordinate (ur) at   (2.5cm,2.5cm);
  \draw (ll) rectangle (ur);
  \clip (ll)+(0.1cm,0.1cm) rectangle ([shift={(-0.1cm,-0.1cm)}]ur);
  \fill[cell color] (ll) rectangle (ur);
  \draw[very thick, white] (ll) grid (ur);
}

\begin{tikzpicture}
  \coordinate (ll) at (-2.5cm,-2.5cm);
  \coordinate (ur) at   (2.5cm,2.5cm);
  \draw (ll) rectangle (ur);
  \clip (ll)+(0.1cm,0.1cm) rectangle ([shift={(-0.1cm,-0.1cm)}]ur);
  \fill[cell color] (ll) rectangle (ur);
  \draw[very thick, white, step=2cm] (ll) grid (ur);

  \begin{scope}[xshift=-2.5cm,yshift=-2.5cm]
    \node at (1,1) {a1};
    \node at (1,2) {$\strut$a2};
    \node at (2,2) {$\strut$\underline{a3}};
    \node at (2,1) {a4};
    \draw[step=1cm, xshift=0.5cm, yshift=0.5cm] (0.1,0.1) grid (1.9,1.9);
    \node[opacity=0.5] at (1.5,1.5) {\Huge{A}};
  \end{scope}

  \begin{scope}[xshift=-2.5cm,yshift=-0.5cm]
    \node at (1,1) {$\strut$b1};
    \node at (1,2) {b2};
    \node at (2,2) {b3};
    \node at (2,1) {$\strut$\underline{b4}};
    \draw[step=1cm, xshift=0.5cm, yshift=0.5cm] (0.1,0.1) grid (1.9,1.9);
    \node[opacity=0.5] at (1.5,1.5) {\Huge{B}};
  \end{scope}

  \begin{scope}[xshift=-0.5cm,yshift=-0.5cm]
    \node at (1,1) {$\strut$\underline{c1}};
    \node at (1,2) {c2};
    \node at (2,2) {c3};
    \node at (2,1) {$\strut$c4};
    \draw[step=1cm, xshift=0.5cm, yshift=0.5cm] (0.1,0.1) grid (1.9,1.9);
    \node[opacity=0.5] at (1.5,1.5) {\Huge{C}};
  \end{scope}
  
  \begin{scope}[xshift=-0.5cm,yshift=-2.5cm]
    \node at (1,1) {d1};
    \node at (1,2) {$\strut$\underline{d2}};
    \node at (2,2) {$\strut$d3};
    \node at (2,1) {d4};
    \draw[step=1cm, xshift=0.5cm, yshift=0.5cm] (0.1,0.1) grid (1.9,1.9);
    \node[opacity=0.5] at (1.5,1.5) {\Huge{D}};
  \end{scope}

  \draw[very thick, step=4cm, xshift=2cm, yshift=2cm ] (ll) grid (ur);
\end{tikzpicture}

\begin{tikzpicture}
  \common
  \draw[very thick, step=2cm] (ll) grid (ur);

  \begin{scope}[xshift=-2.5cm,yshift=-2.5cm]
    \node at (1,1) {A};
    \node at (1,2) {B};
    \node at (2,2) {C};
    \node at (2,1) {D};
  \end{scope}

\end{tikzpicture}

\begin{tikzpicture}
  \common

  \begin{scope}[xshift=-2.5cm,yshift=-2.5cm]
    \node (oG) at (0,0) {};
    \node (oH) at (0,1) {};
    \node (oI) at (0,2) {};
    \node (oJ) at (0,3) {};
    \node (oK) at (1,3) {};
    \node (oL) at (2,3) {};
    \node (oM) at (3,3) {};
    \node (oN) at (3,2) {};
    \node (oO) at (3,1) {};
    \node (oP) at (3,0) {};
    \node (oE) at (2,0) {};
    \node (oF) at (1,0) {};
  \end{scope}

  \begin{scope}[xshift=-1cm,yshift=-1cm, scale=0.5,
    every node/.style={fill=black,inner sep=0, minimum size=0.5cm}]
    \clip (-2,-2) rectangle (2,2);
    \fill[cell color] (-2,-2) rectangle (2,2);
    \draw[step=1cm] (ll) grid (ur);
    \begin{scope}[scale=0.5, white]
      \node at (-1,-1) {A};
      \node at (-1, 1) {B};
      \node at ( 1, 1) {C};
      \node at ( 1,-1) {D};
    \end{scope}
    \draw[white] (-0.90,-0.90) grid (0.90,0.90);

    \begin{scope}[shift={(-1.5,-1.5)},
      every node/.style={inner sep=0, minimum size=2pt}]
      \node  (0) at ( 0, 0) {};
      \node  (1) at ( 0, 3) {};
      \node  (2) at ( 3, 3) {};
      \node  (3) at ( 3, 0) {};
      \node  (4) at ( 0, 1) {};
      \node  (5) at ( 0, 2) {};
      \node  (6) at ( 1, 3) {};
      \node  (7) at ( 2, 3) {};
      \node  (8) at ( 3, 2) {};
      \node  (9) at ( 3, 1) {};
      \node (10) at ( 2, 0) {};
      \node (11) at ( 1, 0) {};
    \end{scope}
  \end{scope}
  \draw[very thick, step=2cm, black!50] (ll) grid (ur);
  \draw[very thick] (-2,-2) rectangle (0,0);

  \draw[-stealth, thick] (oG.center)              to  (0.center);
  \draw[-stealth, thick] (oJ.center)              to  (1.center);
  \draw[-stealth, thick] (oM.center)              to  (2.center);
  \draw[-stealth, thick] (oP.center)              to  (3.center);
  \draw[-stealth, thick] [bend right] (oH.center) to  (4.center);
  \draw[-stealth, thick] [bend left]  (oI.center) to  (5.center);
  \draw[-stealth, thick] [bend right] (oK.center) to  (6.center);
  \draw[-stealth, thick] [bend left]  (oL.center) to  (7.center);
  \draw[-stealth, thick] [bend right] (oN.center) to  (8.center);
  \draw[-stealth, thick] [bend left]  (oO.center) to  (9.center);
  \draw[-stealth, thick] [bend right] (oE.center) to (10.center);
  \draw[-stealth, thick] [bend left]  (oF.center) to (11.center);

\end{tikzpicture}

\begin{tikzpicture}
  \common

  \begin{scope}[xshift=-1cm,yshift=-1cm, scale=0.5,
    every node/.style={fill=black!50,inner sep=0, minimum size=0.5cm}]
    \clip (-2,-2) rectangle (2,2);
    \fill[cell color] (-2,-2) rectangle (2,2);
    \draw[step=1cm, black!50] (ll) grid (ur);
    \begin{scope}[scale=0.5, white]
      \node at (-1,-1) {A'};
      \node at (-1, 1) {B'};
      \node at ( 1, 1) {C'};
      \node at ( 1,-1) {D'};
    \end{scope}
  \end{scope}

  \begin{scope}[xshift=1cm,yshift=1cm, scale=0.5,
    every node/.style={fill=black!50,inner sep=0, minimum size=0.5cm}]
    \clip (-2,-2) rectangle (2,2);
    \fill[cell color] (-2,-2) rectangle (2,2);
    \draw[step=1cm, black!50] (ll) grid ([shift={(-4,-4)}]ur);
    \begin{scope}[scale=0.5, white]
      \node at (-1,-1) {};
    \end{scope}
  \end{scope}

  \begin{scope}[black!50]
    \draw[very thick] (-2,-2) rectangle (0,0);
    \draw[very thick] (0,0) rectangle (2,2);
  \end{scope}

  \draw[very thick, step=2cm, xshift=1cm, yshift=1cm] (ll) grid (ur);

\end{tikzpicture}

\begin{tikzpicture}
  \common

  \begin{scope}[xshift=-1cm,yshift=-1cm, scale=0.5,
    every node/.style={fill=black!50,inner sep=0, minimum size=0.5cm}]
    \clip (-2,-2) rectangle (2,2);
    \fill[cell color] (-2,-2) rectangle (2,2);
    \draw[step=1cm, black!50] (ll) grid (ur);
    \begin{scope}[scale=0.5, white]
      \node (A') at (-1,-1) {A'};
      \node (B') at (-1, 1) {B'};
      \node at ( 1, 1) {C'};
      \node (D') at ( 1,-1) {D'};
    \end{scope}
  \end{scope}

  \begin{scope}[xshift=0cm,yshift=0cm, scale=0.5,
    every node/.style={fill=black!50,inner sep=0, minimum size=0.5cm}]
    \clip (-2,-2) rectangle (0,0);
    \fill[cell color] (-2,-2) rectangle (2,2);
    \draw[step=1cm, black!50] (ll) grid (ur);
    \begin{scope}[scale=0.5, white]
      \node at (-1,-1) {C'};
    \end{scope}
  \end{scope}

  \begin{scope}[xshift=0cm,yshift=0cm, scale=0.5,
    every node/.style={fill=black!50,inner sep=0, minimum size=0.5cm}]
    \clip (0,0) rectangle (2,2);
    \fill[cell color] (-2,-2) rectangle (2,2);
    \draw[step=1cm, black!50] (ll) grid (ur);
    \begin{scope}[scale=0.5, white]
      \node at (1,1) {};
    \end{scope}
  \end{scope}

  \begin{scope}[black!50]
    \draw[very thick] (-2,-2) rectangle (0,0);
    \draw[very thick] (0,0) rectangle (2,2);
  \end{scope}

  \begin{scope}[scale=0.25,xshift=-2cm,yshift=-2cm,
    every node/.style={circle, inner sep=0, minimum size=2pt}]
    \node (A) at (-1,-1) {};
    \node (B) at (-1, 1) {};
    \node (D) at ( 1,-1) {};
    \draw[stealth-, thick]  (A.center) to +( 225:2cm);
    \draw[stealth-, thick]  (B.center) to [bend right] ([shift={(0,0.5)}]B'.center);
    \draw[stealth-, thick]  (D.center) to [bend left ] ([shift={(0.5,0)}]D'.center);
  \end{scope}

  \draw[very thick, step=2cm, xshift=1cm,yshift=1cm] (ll) grid (ur);

\end{tikzpicture}

\begin{tikzpicture}
  \common

  \begin{scope}[xshift=-1cm,yshift=-1cm, scale=0.5,
    every node/.style={black!50, inner sep=0, minimum size=0.5cm},
    important/.style={white, fill=black!50}]
    \clip (-2,-2) rectangle (2,2);
    \fill[cell color] (-2,-2) rectangle (2,2);
    \draw[step=1cm, black!50] (ll) grid (ur);
    \begin{scope}[scale=0.5]
      \begin{scope}[shift={(-2,-2)}]
        \node[important] at (-1,-1) {A'};
        \node            at (-1, 1) {B'};
        \node            at ( 1, 1) {C'};
        \node            at ( 1,-1) {D'};
      \end{scope}
      \begin{scope}[shift={(-2, 2)}]
        \node            at (-1,-1) {A'};
        \node[important] at (-1, 1) {B'};
        \node            at ( 1, 1) {C'};
        \node            at ( 1,-1) {D'};
      \end{scope}
      \begin{scope}[shift={( 2, 2)}]
        \node            at (-1,-1) {A'};
        \node            at (-1, 1) {B'};
        \node[important] at ( 1, 1) {C'};
        \node            at ( 1,-1) {D'};
      \end{scope}
      \begin{scope}[shift={( 2,-2)}]
        \node            at (-1,-1) {A'};
        \node            at (-1, 1) {B'};
        \node            at ( 1, 1) {C'};
        \node[important] at ( 1,-1) {D'};
      \end{scope}
    \end{scope}
  \end{scope}

  \draw[very thick, white] (ll) grid (ur);
  \draw[very thick, step=2cm, xshift=1cm,yshift=1cm] (ll) grid (ur);

\end{tikzpicture}
\end{document}
