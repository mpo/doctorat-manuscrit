\documentclass[crop,tikz]{standalone}
\input{common-headers}
\input{sigles}

\begin{document}

%1
\begin{tikzpicture}
  \node (A) at (0,0) {$\cat{A}$};
  \node (B) at (4,0) {$\cat{B}$};
  \node (C) at (2,0) {$\cat{C}$};
  \draw[-Stealth] (A) to node[auto]      {$S$} (C);
  \draw[-Stealth] (B) to node[auto,swap] {$T$} (C);
\end{tikzpicture}

%2
\begin{tikzpicture}
  \node (sa1) at (0,2) {$S(\alpha)$};
  \node (sa2) at (2,2) {$S(\alpha')$};
  \node (tb2) at (2,0) {$T(\beta')$};
  \node (tb1) at (0,0) {$T(\beta)$};

  \draw[-Stealth] (sa1) to node[auto]      {$S(g)$} (sa2);
  \draw[-Stealth] (sa1) to node[auto,swap] {$f$}    (tb1);
  \draw[-Stealth] (sa2) to node[auto]      {$f'$}   (tb2);
  \draw[-Stealth] (tb1) to node[auto,swap] {$T(h)$} (tb2);
\end{tikzpicture}

%3
\begin{tikzpicture}
  \node (M0) at (2,0) {$M_0$};
  \node (M1) at (1,2) {$M$};
  \node (M2) at (3,2) {$M'$};
  \draw[-Stealth] (M1) to node[auto,swap] {$v_{M}$} (M0);
  \draw[-Stealth] (M2) to node[auto] {$v_{M'}$} (M0);
  \draw[-Stealth] (M1) to node[auto] {$a$} (M2);
\end{tikzpicture}

%4: Produit
\begin{tikzpicture}[node distance=1cm and 2cm]
  \node (P) at (0,0) {$P$};
  \node (X) [left=of P] {$X$};
  \node (Y) [right=of P] {$Y$};
  \node (Q) [above=of P] {$Q$};

  \draw[-Stealth] (P) to node[auto]      {$p_1$}  (X);
  \draw[-Stealth] (P) to node[auto,swap] {$p_2$}  (Y);
  \draw[-Stealth] (Q) to node[auto,swap] {$p'_1$} (X);
  \draw[-Stealth] (Q) to node[auto]      {$p'_2$} (Y);
  \draw[-Stealth,dashed] (Q) to node[auto] {$\exists ! u$} (P);
\end{tikzpicture}

%5 Coproduit
\begin{tikzpicture}[node distance=1cm and 2cm]
  \node (P) at (0,0) {$P$};
  \node (X) [left=of P] {$X$};
  \node (Y) [right=of P] {$Y$};
  \node (Q) [above=of P] {$Q$};

  \draw[-Stealth] (X) to node[auto,swap] {$i_1$}  (P);
  \draw[-Stealth] (Y) to node[auto]      {$i_2$}  (P);
  \draw[-Stealth] (X) to node[auto]      {$i'_1$} (Q);
  \draw[-Stealth] (Y) to node[auto,swap] {$i'_2$} (Q);
  \draw[-Stealth,dashed] (P) to node[auto,swap] {$\exists ! u$} (Q);
\end{tikzpicture}

%6 Produit fibré (1) aka Pullback
\begin{tikzpicture}
  \node (X) at ( 0,0) {$X$};
  \node (Y) at ( 2,2) {$Y$};
  \node (Z) at ( 2,0) {$Z$};
  \node (P) at ( 0,2) {$P$};

  \draw[-Stealth] (X) to node[auto,swap] {$f$}   (Z);
  \draw[-Stealth] (Y) to node[auto]      {$g$}   (Z);
  \draw[-Stealth] (P) to node[auto,swap] {$p_1$} (X);
  \draw[-Stealth] (P) to node[auto]      {$p_2$} (Y);
\end{tikzpicture}


%7 Produit fibré (2) Pullback
\begin{tikzpicture}
  \node (X) at ( 0,0) {$X$};
  \node (Y) at ( 2,2) {$Y$};
  \node (Z) at ( 2,0) {$Z$};
  \node (P) at ( 0,2) {$P$};
  \node (Q) at (-1,3) {$Q$};

  \draw[-Stealth] (X) to node[auto,swap] {$f$}   (Z);
  \draw[-Stealth] (Y) to node[auto]      {$g$}   (Z);
  \draw[-Stealth] (P) to node[auto,swap] {$p_1$} (X);
  \draw[-Stealth] (P) to node[auto]      {$p_2$} (Y);
  \draw[-Stealth,bend right] (Q) to node[auto,swap] {$p'_1$} (X);
  \draw[-Stealth,bend left]  (Q) to node[auto]      {$p'_2$} (Y);
  \draw[-Stealth,dashed] (Q) to node[auto]   {$\exists ! u$} (P);
\end{tikzpicture}

%8 Somme amalgamée (1) aka Pushout
\begin{tikzpicture}
  \node (X) at ( 0,0) {$X$};
  \node (Y) at ( 2,2) {$Y$};
  \node (Z) at ( 2,0) {$Z$};
  \node (P) at ( 0,2) {$P$};

  \draw[-Stealth] (Z) to node[auto] {$f$}        (X);
  \draw[-Stealth] (Z) to node[auto,swap] {$g$}   (Y);
  \draw[-Stealth] (X) to node[auto]      {$i_1$} (P);
  \draw[-Stealth] (Y) to node[auto,swap] {$i_2$} (P);
\end{tikzpicture}

%9 Somme amalgamée (2) aka Pushout
\begin{tikzpicture}
  \node (X) at ( 0,0) {$X$};
  \node (Y) at ( 2,2) {$Y$};
  \node (Z) at ( 2,0) {$Z$};
  \node (P) at ( 0,2) {$P$};
  \node (Q) at (-1,3) {$Q$};

  \draw[-Stealth] (Z) to node[auto] {$f$}        (X);
  \draw[-Stealth] (Z) to node[auto,swap] {$g$}   (Y);
  \draw[-Stealth] (X) to node[auto]      {$i_1$} (P);
  \draw[-Stealth] (Y) to node[auto,swap] {$i_2$} (P);
  \draw[-Stealth,bend left]  (X) to node[auto]      {$i'_1$} (Q);
  \draw[-Stealth,bend right] (Y) to node[auto,swap] {$i'_2$} (Q);
  \draw[-Stealth,dashed] (P) to node[auto,swap]   {$\exists ! u$} (Q);
\end{tikzpicture}

%10 Slice catégorie
\begin{tikzpicture}[node distance=1.2cm and 1cm]
  \node (X) at (0,0) {$X$};
  \node (Y1) [above left =of X] {$Y1$};
  \node (Y2) [above right=of X] {$Y2$};

  \draw[-Stealth] (Y1) to node[auto] {$g$} (Y2);
  \draw[-Stealth] (Y1) to node[auto,swap] {$f_1$} (X);
  \draw[-Stealth] (Y2) to node[auto] {$f_2$} (X);
\end{tikzpicture}

%11
\begin{tikzpicture}
  \node (M1)  at (-5,0) {$\model{M}{1}$};
  \node (M12) at (-3,0) {$\model{M}{12}$};
  \node (M2)  at (-1,0) {$\model{M}{2}$};
  \node (M)   at (-3,2) {$\modelM'$};

  \node (equiv)   at (0,0) {$\Leftrightarrow$};

  \node (E1)  at (1,0) {$E_{\model{M}{1}}$};
  \node (E12) at (3,0) {$E_{\model{M}{12}}$};
  \node (E2)  at (5,0) {$E_{\model{M}{2}}$};
  \node (ER)  at (3,-2) {$E_{\model{M}{S}}$};
  \node (EM)  at (3,2) {$E_{\modelM'}$};


  \draw[-Stealth] (M12) to node[auto,swap] {$\absA_1$} (M1);
  \draw[-Stealth] (M12) to node[auto] {$\absA_2$} (M2);
  \draw[-Stealth,dashed] (M) to node[auto] {$\absA'$} (M12);
  \draw[-Stealth] (M) to node[auto,swap] {$\absA'_1$} (M1);
  \draw[-Stealth] (M) to node[auto] {$\absA'_2$} (M2);

  \draw[-Stealth] (E1) to node[auto] {$f_{\absA_1}$} (E12);
  \draw[-Stealth] (E2) to node[auto,swap] {$f_{\absA_2}$} (E12);
  \draw[-Stealth,dashed] (E12) to node[auto,swap] {$f_{\absA'}$} (EM);
  \draw[-Stealth] (E1) to node[auto] {$f_{\absA'_1}$} (EM);
  \draw[-Stealth] (E2) to node[auto,swap] {$f_{\absA'_2}$} (EM);
  \draw[-Stealth] (ER) to node[auto] {$\sigma_{\model{M}{1}}$} (E1);
  \draw[-Stealth] (ER) to node[auto,swap] {$\sigma_{\model{M}{2}}$} (E2);
\end{tikzpicture}

%12
\begin{tikzpicture}
  \node (M1)  at (-5,0) {$\model{M}{1}$};
  \node (M12) at (-3,2) {$\model{M}{12}^0$};
  \node (M2)  at (-1,0) {$\model{M}{2}$};
  \node (M0)  at (-3,0) {$\modelM_0$};

  \node (equiv)   at (0,0) {$\Leftrightarrow$};

  \node (E1)  at (1,0) {$E_{\model{M}{1}}$};
  \node (E12) at (3,2) {$E_{\model{M}{12}^0}$};
  \node (E2)  at (5,0) {$E_{\model{M}{2}}$};
  \node (ER)  at (3,-2) {$E_{\model{M}{S}}$};
  \node (EM)  at (3,0) {$E_{\modelM_0}$};

  \draw[-Stealth] (M12) to node[auto,swap] {$\absA_1$} (M1);
  \draw[-Stealth] (M12) to node[auto] {$\absA_2$} (M2);
  \draw[-Stealth] (M1) to node[auto] {$\absA^0_1$} (M0);
  \draw[-Stealth] (M2) to node[auto,swap] {$\absA^0_2$} (M0);

  \draw[-Stealth] (E1) to node[auto] {$f_{\absA_1}$} (E12);
  \draw[-Stealth] (E2) to node[auto,swap] {$f_{\absA_2}$} (E12);
  \draw[-Stealth] (EM) to node[auto,swap] {$f_{\absA^0_1}$} (E1);
  \draw[-Stealth] (EM) to node[auto] {$f_{\absA^0_2}$} (E2);
  \draw[-Stealth] (ER) to node[auto] {$\sigma_{\model{M}{1}}$} (E1);
  \draw[-Stealth] (ER) to node[auto,swap] {$\sigma_{\model{M}{2}}$} (E2);
  \draw[-Stealth] (ER) to node[auto,swap] {$\sigma_{\model{M}{0}}$} (EM);
\end{tikzpicture}

%13
\begin{tikzpicture}
  \node (Mp) at (-3,1) {$\model{M}{+}$};
  \node (Mm) at (-1,1) {$\model{M}{-}$};
  \node (eq) at (0,1) {$\Leftrightarrow$};
  \node (Er) at (2,0) {$E_{\model{M}{S}}$};
  \node (Ep) at (1,2) {$E_{\model{M}{+}}$};
  \node (Em) at (3,2) {$E_{\model{M}{-}}$};
  \draw[-Stealth] (Mp) to node[auto] {$\absA$} (Mm);
  \draw[-Stealth] (Er) to node[auto] {$\sigma_{\model{M}{+}}$} (Ep);
  \draw[-Stealth] (Er) to node[auto,swap] {$\sigma_{\model{M}{-}}$} (Em);
  \draw[-Stealth] (Em) to node[auto,swap] {$f_\absA$} (Ep);
\end{tikzpicture}

%14
\begin{tikzpicture}
  \node (Mp) at (-3,2) {$\model{M}{+}$};
  \node (Mm) at (-1,2) {$\model{M}{-}$};
  \node (eq) at (0,2) {$\Leftrightarrow$};
  \node (Er) at (2,0) {$E_{\model{M}{S}}$};
  \node (Ep) at (1,2) {$E_{\model{M}{+}}$};
  \node (Em) at (3,2) {$E_{\model{M}{-}}$};
  \node (eq) at (4,2) {$\stackrel{\ftr{U}_\cat{AMon}}\mapsfrom$};
  \node (ap) at (5,2) {$\Phi_+$};
  \node (am) at (7,2) {$\Phi_-$};

  \draw[-Stealth] (Mp) to node[auto] {$\absA$} (Mm);
  \draw[-Stealth] (Er) to node[auto] {$\sigma_{\model{M}{+}}$} (Ep);
  \draw[-Stealth] (Er) to node[auto,swap] {$\sigma_{\model{M}{-}}$} (Em);
  \draw[-Stealth] (Em) to node[auto,swap] {$f_\absA$} (Ep);
  \draw[-Stealth] (am) to node[auto,swap] {$h$} (ap);
\end{tikzpicture}

%15
\begin{tikzpicture}
  \node (M1)  at (-2,0) {$\model{M}{1}$};
  \node (M12) at (0,0) {$\model{M}{12}$};
  \node (M2)  at (2,0) {$\model{M}{2}$};
  \node (M)   at (0,2) {$\modelM'$};

  \draw[-Stealth] (M1) to node[auto] {$\absA_1$} (M12);
  \draw[-Stealth] (M2) to node[auto,swap] {$\absA_2$} (M12);
  \draw[-Stealth,dashed] (M12) to node[auto,swap] {$\absA'$} (M);
  \draw[-Stealth] (M1) to node[auto] {$\absA'_1$} (M);
  \draw[-Stealth] (M2) to node[auto,swap] {$\absA'_2$} (M);
\end{tikzpicture}

%16
\begin{tikzpicture}
  \node (M1)  at (-2,0) {$E_{\model{M}{1}}$};
  \node (M12) at (0,0) {$E_{\model{M}{12}}$};
  \node (M2)  at (2,0) {$E_{\model{M}{2}}$};
  \node (M)   at (0,2) {$E_{\modelM'}$};
  \node (R)   at (0,-2) {$E_{\model{M}{S}}$};

  \draw[-Stealth] (M12) to node[auto,swap] {$f_{\absA_1}$} (M1);
  \draw[-Stealth] (M12) to node[auto] {$f_{\absA_2}$} (M2);
  \draw[-Stealth,dashed] (R) to node[auto,swap] {$\sigma_{\model{M}{12}}$} (M12);
  \draw[-Stealth] (R) to node[auto] {$\sigma_{\model{M}{1}}$} (M1);
  \draw[-Stealth] (R) to node[auto,swap] {$\sigma_{\model{M}{2}}$} (M2);
  \draw[-Stealth] (M) to node[auto,swap] {$f_{\absA'_1}$} (M1);
  \draw[-Stealth] (M) to node[auto] {$f_{\absA'_2}$} (M2);
  \draw[-Stealth,dashed] (M) to node[auto] {$f_{\absA'}$} (M12);
\end{tikzpicture}

%17
\begin{tikzpicture}
  \node (M1)  at (-5,0) {$\model{M}{1}$};
  \node (M12) at (-3,0) {$\model{M}{12}^0$};
  \node (M2)  at (-1,0) {$\model{M}{2}$};
  \node (M0)  at (-3,2) {$\model{M}{0}$};

  \node (equiv)   at (0,0) {$\Leftrightarrow$};

  \node (E1)  at (1,0) {$E_{\model{M}{1}}$};
  \node (E12) at (3,0) {$E_{\model{M}{12}^0}$};
  \node (E2)  at (5,0) {$E_{\model{M}{2}}$};
  \node (ER)  at (3,-2) {$E_{\model{M}{S}}$};
  \node (EM)  at (3,2) {$E_{\model{M}{0}}$};


  \draw[-Stealth] (M1) to node[auto] {$\absA_1$} (M12);
  \draw[-Stealth] (M2) to node[auto,swap] {$\absA_2$} (M12);
  %%\draw[-Stealth,dashed] (M0) to node[auto] {$\absA'$} (M12);
  \draw[-Stealth] (M0) to node[auto,swap] {$\absA^0_1$} (M1);
  \draw[-Stealth] (M0) to node[auto] {$\absA^0_2$} (M2);

  \draw[-Stealth] (E12) to node[auto,swap] {$f_{\absA_1}$} (E1);
  \draw[-Stealth] (E12) to node[auto] {$f_{\absA_2}$} (E2);
  %\draw[-Stealth,dashed] (E12) to node[auto,swap] {$f_{\absA'}$} (EM);
  \draw[-Stealth] (E1) to node[auto] {$f_{\absA^0_1}$} (EM);
  \draw[-Stealth] (E2) to node[auto,swap] {$f_{\absA^0_2}$} (EM);
  \draw[-Stealth] (ER) to node[auto] {$\sigma_{\model{M}{1}}$} (E1);
  \draw[-Stealth] (ER) to node[auto,swap] {$\sigma_{\model{M}{2}}$} (E2);
  \draw[-Stealth,dashed] (ER) to node[auto,swap] {$\sigma_{\model{M}{12}^0}$} (E12);
\end{tikzpicture}


\end{document}
