\documentclass{standalone}
\input{common-headers}
\input{sigles}
\begin{document}
\begin{tikzpicture}[scale=0.9,
  shf/.style={isosceles triangle, shape border rotate=90, minimum height=0.5cm,
    minimum width=.7cm, fill=red!30!white, isosceles triangle stretches},
  she/.style={fill=black, rectangle, inner xsep=0.3cm, inner ysep=0.06cm},
  shc/.style={fill=black, circle, inner sep=0.1cm},
  2cell/.style={fill=red!30!white},
  1cell/.style={draw=black, ultra thick},
  0cell/.style={shape=circle, fill=black, draw=white, ultra thick, inner sep=0.1cm}]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Figure on the left
\begin{scope}[xshift=-6cm, yshift=1.55cm,every node/.style={draw=white, ultra thick, inner sep=0}]
%nodes
\node[shf] (f)  [label=above:$f$] {};
\node[she] (e2) [label=right:$e_2$,below=of f] {};
\node[she] (e1) [label=right:$e_1$,left=of e2] {};
\node[she] (e3) [label=right:$e_3$,right=of e2] {};
\node[shc] (c2) [label=below:$c_2$,below=of e1] {};
\node[shc] (c1) [label=below:$c_1$,below=of e2] {};
\node[shc] (c3) [label=below:$c_3$,below=of e3] {};

%incidence
\draw (f) -- (e1);\draw (f) -- (e2);\draw (f) -- (e3);
\draw (e1) -- (c1);\draw (e1) -- (c2);
\draw (e2) -- (c2);\draw (e2) -- (c3);
\draw (e3) -- (c1);\draw (e3) -- (c3);
\end{scope}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Figure on the center
\begin{scope}[scale=0.8]
\fill[2cell] (90:2cm) -- (210:2cm) -- (330:2cm);
\node at (0,0) {$f$};
\end{scope}

\draw[1cell] (90 :2cm) -- node[above left] {$e_1$} (210:2cm);
\draw[1cell] (210:2cm) -- node[below] {$e_2$} (330:2cm);
\draw[1cell] (330:2cm) -- node[above right] {$e_3$} ( 90:2cm);

\node[0cell,label=above:$c_1$] at ( 90:2cm) {};
\node[0cell,label=left:$c_2$]  at (210:2cm) {};
\node[0cell,label=right:$c_3$] at (330:2cm) {};

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Figure on the right
\begin{scope}[xshift=6cm]
\begin{scope}[scale=0.8]
\fill[2cell] (90:2cm) -- (210:2cm) -- (330:2cm);
\node at (0,0) {$12$};
\end{scope}

\draw[1cell] (90 :2cm) -- node[above left]  {$5$} (210:2cm);
\draw[1cell] (210:2cm) -- node[below]       {$6$} (330:2cm);
\draw[1cell] (330:2cm) -- node[above right] {$5$} ( 90:2cm);

\node[0cell,label=above:{$(0,4)$}] at ( 90:2cm) {};
\node[0cell,label=below:{$(-3,0)$}] at (210:2cm) {};
\node[0cell,label=below:{$(3,0)$}] at (330:2cm) {};
\end{scope}
\end{tikzpicture}
\end{document}
