#!/bin/sh

zcat fullStablePopulation.data.gz \
  | nl \
  | awk 'NR == 0 || NR % 1000 == 0' \
  > stablePopulation.data
