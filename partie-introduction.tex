\chapter{Introduction}%\minitoc

%Intro barrée
Depuis l'avènement de la science informatique, les modèles sont de plus en plus
utilisés pour décrire et comprendre le monde qui nous entoure. Automatisés et
portés par les langages de programmation, ils sont de plus en plus imposants,
au sens où ils incorporent de plus en plus d'information et visent à être de
plus en plus complets. Barrant le chemin à une meilleure compréhension de notre
environnement, la modélisation des \emph{systèmes complexes} est le prochain col
à franchir dans la compréhension du vivant (pour les sciences du vivant) et dans
la compréhension des fondements des modèles (pour les sciences informatiques et
mathématiques).

Il existe un riche vocabulaire entourant le monde des \emph{systèmes
complexes}: émergence, immergence, backward- et forward-causality, …
Ces notions sont difficiles à appréhender car elles se fondent sur
des cas particuliers (comme les exemples classiques du jeu de la
vie~\cite{gardner_mathematical_1970}, des boids~\cite{reynolds_flocks_1987} ou
de la fourmilière~\cite{wilensky_netlogo_1997}) et sur un certain laxisme formel
dans la description des modèles, provenant de la confusion entre les objets que
le modèle manipule et de notre propre identification de ces mêmes objets. Pour
chacun des exemples donnés ci-dessus, nous pouvons nommer des propriétés qui ne
font pas partie du modèle:
\begin{itemize}
  \item Dans le jeu de la vie, un observateur parlera souvent de «glider», de
    «canons» à glider, de «beacon», de «pulsar», etc.;
  \item Dans le modèles des boids, un observateur identifiera un comportement
    de groupe et cherchera à en identifier le «leader»;
  \item Dans le modèle de la fourmilière, un observateur remarquera que les
    individus sélectionnent le «plus court chemin» pour rapporter la nourriture
    au nid, et qu'ils traitent les sources de nourriture dans l'ordre «du plus
    proche au plus lointain».
\end{itemize}
Toutes les propriétés mises entre guillemets ci-dessus ne sont pas des
propriétés des modèles présentés, il n'y a pas de «glider» dans le modèle du
jeu de la vie, il n'y a pas de «leader» dans le modèle des boids ni de «plus
court chemin» dans le modèle de la fourmilière. Ce sont des interprétations qui
sont faites sur le déroulement d'une simulation, des propriétés d'un modèle
\emph{implicite} que l'observateur établit de lui-même. Rendre explicite ce
modèle puis lier formellement ces propriétés émergentes au modèle d'origine est
un enjeu de taille, car il permettrait de relier tout ensemble de propriétés
\emph{locales} à des propriétés \emph{globales}, c'est-à-dire des propriétés des
individus à des propriétés du groupe formé de ces individus.

% Loi / simulation | global / local
Un outil classique de la modélisation, les équations différentielles, nous
donne l'occasion d'illustrer ce rapport local/global. Un système d'équations
différentielles permet de décrire le comportement d'entités locales (au cours
du temps, les unes par rapport aux autres, …). Lorsque ce système d'équations
différentielles possède une solution, alors la fonction solution est une
\emph{loi globale} du modèle établi au niveau local. Il existe un lien formel
entre le comportement local de ces entités et leur comportement global. Il
est possible d'étudier cette fonction \emph{directement} en toute généralité.
Toutefois, cette solution au système d'équations différentielles n'existe pas
forcément. Dans ce cas, il est toujours possible d'obtenir le comportement
global de ces entités par intégration numérique, c'est-à-dire en approchant
la fonction solution \emph{au cas par cas}. N'ayant pas accès à la fonction
solution, il n'est cependant pas possible d'étudier directement le comportement
global de cette population.

%equadiff avec solution = loi (fast path)
%equadiff sans solution = simulation (slow path)

% Le multi-modèle à la rescousse ?
Nous prendrons partie pour le fait que construire un modèle constitué de
plusieurs sous-modèles nous permet sous certaines conditions de répondre à la
problématique. Cela doit nous permettre de concilier les comportements locaux et
globaux de ce système. Nous appellerons cette technique la \emph{modélisation
multi-niveau}, que nous considérons comme une sous-branche de la modélisation
multi-modèle.

La modélisation multi-modèle consiste à conjuguer différents modèles
d'un système, où chacun des modèles décrit une partie fonctionnelle ou
structurelle du système. Ces modèles, développés indépendamment, sont parfois
rédigés dans des formalismes si distincts qu'il est difficile de les faire
collaborer. Un exemple de discipline reposant sur cette problématique est la
\emph{mécatronique}~\cite{bishop_mechatronics_2002}, dont le sujet d'étude
est le couplage entre systèmes mécaniques et électroniques. La modélisation
multi-niveau s'intéresse aux modélisations multi-modèle ayant les propriétés
suivantes:
\begin{itemize}
  \item Le système peut se représenter comme un \emph{empilement} de points
    de vue appelés \emph{niveaux de description}. Même si ces niveaux ne
    sont pas agencés dans un ordre total, il existe cependant une relation
    caractérisable entre eux, comme par exemple une relation d'abstraction.
  \item Un niveau de description du système peut n'être que partiellement
    connu et c'est par leur couplage que la modélisation du système se trouve
    \emph{renforcée}~\cite{banos_coupling_2015}.
\end{itemize}
Sans entrer dans les détails, nous considérerons qu'un modèle est réductionniste
s'il peut être découpé en sous-parties autonomes permettant, une fois assemblées
de décrire exactement son comportement. La construction précédente permet de
garder une vision réductionniste des parties du système, tout en indiquant que
parfois il n'est pas possible de fournir un modèle réductionniste du système
entier, notamment à cause d'un manque de connaissance sur le système.

% Introduction trop longue, quand est-ce qu'on tappe ?
Nos travaux sont issus de réflexions sur la modélisation multi-niveau,
l'activité comme un niveau de description dans un langage de programmation
spatiale et la simulation de la morphogénèse dans de grandes populations de
bactéries, ayant pour origine les motivations du projet ANR Blanc \synbiotic.


\section{Le projet \synbiotic}\label{sec:synbiotic}
Dans cette section, nous présentons le projet ANR \synbiotic\footnote{La
page de présentation du projet est disponible en ligne à l'adresse
\url{http://synbiotic.spatial-computing.org}}.

Le projet de recherche \synbiotic vise à développer des formalismes et
des outils informatiques permettant de spécifier un comportement spatial
global et de le compiler automatiquement à travers une tour de langages
intermédiaires dans des processus locaux de régulation cellulaire (régulation
génétique, métabolique, signalisation). La motivation finale est de permettre
l'exploitation des propriétés collectives d'une population bactérienne pour
créer des biosystèmes artificiels répondant à divers besoins dans le domaine de
la santé, des nanotechnologies, de l'énergie et de la chimie.

\synbiotic s'inscrit dans le domaine des langages de programmation non
conventionnels et de l'analyse de propriétés des systèmes dynamiques, à
l'interface de l'informatique et de l'ingénierie biologique. Il s'appuie sur
les avancées de la biologie synthétique, les progrès réalisés dans la
modélisation et la simulation de processus biologiques complexes, et sur le
développement de nouvelles approches de la programmation permettant de faire
face à de nouvelles classes d'application caractérisées par l'émergence d'un
comportement global dans une grande population d'entités irrégulièrement et
dynamiquement connectées (le calcul amorphe et le calcul autonome).

La biologie synthétique est un domaine scientifique qui concerne la conception
et la fabrication banalisée et standardisée de composants et de systèmes
biologiques sans correspondants naturels. Elle est toujours en quête de
principes de conception permettant une réalisation fiable et sécurisée à partir
de composants biologiques réutilisables.

Dans ce contexte, l'objectif est de concevoir et développer les outils
permettant de « compiler » (au sens de la compilation des langages de
programmation) le comportement global d'une population, par exemple, de
bactéries, en des processus cellulaires locaux à chaque entité. Notre motivation
à très long terme est de permettre l'exploitation des propriétés collectives
d'une population (bactérienne) pour créer des biosystèmes artificiels répondant
à divers besoins dans le domaine de la santé, des nanotechnologies, de l'énergie
et de la chimie verte. L'approche originale que nous proposons se fonde sur une
tour de langages de programmation, dont l'étage le plus abstrait définit un
modèle computationnel pour une population cellulaire et l'étage le plus primitif
correspond à un agencement de séquences d'ADN. Chaque langage compile ses
constructions propres vers le langage de la couche inférieure et ce, jusqu'au
bioware (le « hardware biologique »). Cette approche, similaire à celle suivie
avec succès dans le domaine de la synthèse d'architecture matérielle (chaîne
de compilation vers le silicium), permet de combler le fossé existant entre la
description d'un système au niveau d'abstraction pertinent pour l'application
et la prise en compte de tous les détails de son implantation par des processus
physico-chimiques. Elle permet de modulariser la conception d'un système,
divisant les difficultés et isolant des niveaux d'abstraction qui peuvent
évoluer indépendamment. Dans cette approche, un programme ne définit pas une
fonction qui associe une sortie à une entrée mais spécifie un système dynamique
(biologique) distribué qui essaie de maintenir des invariants en dépit des
perturbations et des changements de l'environnement.

Notre objectif est d'adresser la conception de grands systèmes biologiques
par une approche langage, de la même manière que VHDL permet la conception
de système de traitement de l'information à partir de portes et de blocs
logiques élémentaires. Ce projet informatique repose sur trois hypothèses :
l'apport des formalismes discrets, un processus de conception fondé sur la
compilation d'une tour de langages et la prise en compte des aspects spatiaux.

Notre première hypothèse est que des modèles informatiques discrets sont
adéquats pour décrire des biosystèmes et parfois plus pertinents que des
approches mathématiques traditionnelles comme les équations différentielles.
Cette hypothèse est corroborée par l'important développement actuel des
formalismes informatiques dans le domaine de la biologie des systèmes. En
particulier, ces formalismes sont plus à même de capturer de manière concise
les aspects qualitatifs et quantitatifs des grands réseaux d'interactions
biochimiques impliqués dans les processus biologiques. Ces formalismes
permettent de découpler les abstractions utilisés dans le processus de
conception (signal, gradient, mémoire, propagation, information de position…)
des processus biochimiques utilisés pour leur implémentation, de la même
manière qu'un bit abstrait de manière robuste une implantation électrique
dans une électronique à base de silicium. Par ailleurs, ces formalismes
permettent d'aborder la question de la validation : que peut-on garantir sur les
comportements du système biologique artificiel, quel est le domaine de viabilité
du système, quels sont les perturbations de l'environnement qui sont tolérables,
quels est la résilience du système, peut-on garantir que certains états sont
inatteignables, tracer les processus, tester les comportements attendus, etc.

Notre seconde hypothèse est qu'à partir de ces formalismes, la compilation est
une approche descendante plus souple que l'assemblage direct de composants
biologiques prédéfinis. Le processus de compilation permet d'instancier des
composants élémentaires génériques dans un organisme particulier, permet de
prendre en compte des contraintes d'assemblage (comme l'évitement de cross-talk
entre circuits de régulation) ainsi que la simplification et l'optimisation
des circuits obtenus par assemblage. Cette approche de haut-niveau correspond
à la synthèse d'un système à partir de ses spécifications et repose sur la
possibilité de dériver le comportement des parties à partir du comportement
d'un tout. Ce problème est notoirement plus simple que celui de l'inférence de
propriétés globales à partir de comportements locaux (émergence) et a montré
toute son utilité dans le domaine de la synthèse d'architecture, mais aussi dans
le cadre de la programmation spatiale et de la programmation amorphe. Un des
enjeux du projet est de montrer que ces techniques peuvent être appliquées avec
succès à la synthèse de biosystèmes.

Enfin, notre dernière hypothèse est que, même si pour l'instant la biologie
synthétique se focalise sur la « programmation d'une seule bactérie »,
le développement de biosystèmes un tant soit peu complexe reposera sur
le fonctionnement intégré de colonies bactériennes et donc sur la prise
en compte d'interactions spatiales au sein d'une population de cellules
différenciées. Il est en effet douteux qu'une cellule puisse supporter un
nombre arbitraire de comportements artificiellement imposés. Au contraire,
l'exemple des processus biologiques naturels montrent toute l'importance de
l'organisation spatiale et de la compartimentalisation (membrane, vésicule,
cargo, compartiment, cellule, biofilm, tissus, organe, etc.) permettant la
spécialisation et le fonctionnement intégré au sein d'un système compris
comme une écologie. Par ailleurs, la maîtrise des interactions spatiales
ouvre la voie à une ingénierie du développement (« développement » au
sens biologique du terme), ce qui permet de rêver à des applications qui vont
bien au-delà de la conception de la cellule comme « usine chimique ».
%

\paragraph{Découpage des tâches.}
Le projet \synbiotic est découpé en six \emph{Work Packages} (WP), dont les WP
1, 2, 3 et 4 forment une tour de langages reliant le hardware/wetware (WP4) au
langage de spécification global (WP1). Voici une description de ces quatre
tâches:
\begin{description}
  \item[WP1: Ingénierie des formes] ce WP est en charge de concevoir, de
    développer et d'implémenter des exemples d'applications qui pourront être
    utilisées par les autres WP.
  \item[WP2: Programmation spatiale déclarative] ce WP est en charge de
    l'élaboration d'un langage de programmation spatiale L1, dont la fonction
    est de décrire les exemples du WP1 (définis en terme de primitives spatiales
    sur des populations) vers un langage de plus bas niveau servant d'entrée au
    WP3.
  \item[WP3: Programmation orientée entité] ce WP est en charge de l'élaboration
    d'un nouveau langage L2 prenant en entrée les concepts utilisés en sortie de
    L1 et les instanciant dans les individus (des bactéries).
  \item[WP4: Programmation orientée réseau de régulation] l'enjeu de ce WP est
    de permettre la traduction de la sortie de L2 vers un médium biologique,
    afin d'obtenir une implémentation finale \emph{in vivo}.
\end{description}
Les WP 5 et 6 sont transversaux et se chargent de «calculabilité et complexité»
et de «Bio-sûreté et bio-sécurité» respectivement.


 \section{Objectifs et organisation de cette thèse}
Dans cette section nous présentons brièvement les objectifs de ce manuscrit et
les chapitres qui le composent afin de donner au lecteur une vue d'ensemble des
résultats obtenus.

Comme nous l'avons vu, nos travaux sont ancrés dans le contexte du projet
\synbiotic. Nous avons contribué aux WP2 (chapitre 3) et au WP3 (chapitre 4).
Nous avons également dépassé le cadre du projet en nous intéressant de plus près
à la nature des modèles (mathématiques) et à la définition de la modélisation
multi-niveau.

\paragraph{Chapitre 2: Une approche formelle générale des niveaux de
  modélisation}
Dans ce chapitre nous proposons une première voie de réponse à notre
problématique en nous attaquant au problème de la définition à la fois
formelle et générique des niveaux de modélisation. Nous introduisons une
définition claire de ce qu'est un \emph{modèle formel} qui nous permet de
présenter explicitement notre point de vue. Une fois ce socle à disposition,
nous présentons différentes classes de modèles avec leurs exemples dans notre
formalisme mettant en évidence la possibilité d'identifier ces classes de
modèles suivant plusieurs critères reposant sur la structure mathématique
associée à leur comportement. Nous proposons enfin une construction de
niveaux d'abstraction, reposant sur quelques principes issus de la théorie
des catégories, introduisant la validation, l'abstraction et la composition
des modèles dans ce cadre. Nous instancions finalement sur un exemple les
définitions précédentes mettant en lumière les relations existantes entre quatre
modèles d'un système proie-prédateur.

\paragraph{Chapitre 3: L'activité spatiale comme outil de modélisation}
Dans ce chapitre nous présentons le langage L1 du WP2 en posant un cadre
pratique et théorique porté par le projet \MGS. Ce dernier développe un langage
de programmation dédié à la modélisation et la simulation de systèmes dynamiques
à structure dynamique. L'état du système est décrit à travers une structure
de données — la collection topologique — qui met l'accent sur les relations
topologiques entre les éléments du système. L'évolution du système est spécifiée
au moyen d'une structure de contrôle — la transformation — qui décrit, sous
forme de règles \emph{locales}, les interactions entre les éléments du système.
Nous présentons l'activité spatiale dans le contexte de \MGS, qui nous indique
la répartition dans une collection topologique de la sous-collection active
et nous implémentons un algorithme nous permettant de mettre à jour cette
collection sans connaissance de la sous-collection quiescente. Au delà de
l'optimisation en temps de calcul qu'il apporte à certaines simulation, cet
outil nous permet de capturer une \emph{propriété d'ordre supérieur} du modèle
et offre une voie pour la description, dans \MGS, de modèle à plusieurs niveaux
de description.

\paragraph{Chapitre 4: Un exemple concret de simulation multi-niveaux} Dans
ce chapitre nous ouvrons une troisième voie de réponse à notre problématique,
tournée vers la pratique, où nous présentons la conception d'un simulateur de
populations de bactéries \Ecoli, nommé \otb, correspondant au langage L2 du WP3
de \synbiotic. En effet, l'évolution de ces populations de bactéries montre
un fort potentiel pour la modélisation et la simulation multi-niveau: une
population suffisamment importante mène à des propriétés définies uniquement
à l'échelle de cette population, comme l'émergence et le maintient de formes
particulières — la morphogénèse. Le but de ce simulateur n'est pas, comme dans
les chapitres précédents, de fournir une méthode générique pour exprimer ces
propriétés mais plutôt de partir de la bactérie définie individuellement et
de tester, donc de mettre en évidence, le lien existant entre les propriétés
de l'individu (le réseau de régulation génétique) et les propriétés de la
population (les formes émergentes). Pour relever les défis posés par la
simulation d'un grand nombre d'individus, nous tirons partie du calcul parallèle
sur des cartes graphiques grand public et, dans ce cadre, nous avons développé
plusieurs algorithmes originaux pour le calcul parallèle dont le principal est
\ppm, inspiré des automates cellulaires et du voisinage de Margolus.


\section{Contributions}

Voici une liste succincte des contributions scientifiques apportées par cette
thèse.

\subsection*{Théorie et formalisation}
\begin{itemize}
  \item établissement d'un cadre formel pour l'expression unifiée des niveaux
    de modélisation, indépendamment de leur formalisme d'origine;
  \item définition de l'activité spatiale;
  \item élaboration d'un algorithme pour l'identification et le maintient d'une
    zone spatialement active dans \MGS;
  \item définition d'un algorithme générique (\ppm) pour le calcul parallèle.
\end{itemize}

\subsection*{Développement}
\begin{itemize}
  \item développement d'un simulateur pour mettre en évidence l'émergence et le
    maintient de formes dans une population de bactéries \ecoli.
\end{itemize}

\subsection*{Communications orales}
\begin{itemize}
\item présentation de «Computing Activity in Space» au workshop SCW13 de la
  conférence AAMAS 2013 à St Paul (Minnesota, USA), du 6 au 10 mai 2013;
\item présentation de «Topological Computation of Activity Regions» au workshop
  Work In Progress de la conférence SIGSIM PADS 2013 à Montréal (Québec,
  Canada), du 19 au 22 mai 2013;
\item présentation au séminaire du LACL, intitulé «Implementing Multiple Levels
  of Organization in a Spatial Programming Language» le 10 février 2014, à
  Créteil;
\item participation au workshop SCW14 attaché à la conférence AAMAS qui s'est
  tenu à Paris du 5 au 9 mai 2014;
\item participation au workshop 228 de l'INSERM intitulé : «Experimental
  approaches in mechanotransduction: from molecules to issues and pathology»,
  du 21 au 23 Mai à Bordeaux, France;
\item présentation d'un tutoriel \MGS à la conférence ICCSA 2014, le 24 Juin
  au Havre, France;
\item présentation de «Managing the Interoperability Between Models of a Complex
  System» au workshop 6 satellite de la conférence ICCSA 2014, du 23 au 26 Juin
  au Havre, France;
\item présentation de «Spatial Computing for Integrative Modeling» au ComBio à
  Turku, Finlande, le 29 octobre 2015.
\end{itemize}

\subsection*{Communications écrites} Les travaux présentés dans ce manuscrit
ont donné lieu à plusieurs publications dans des conférences internationales
et nationale~\cite{potier_computing_2013, potier_topological_2013,
pascalie_developmental_2016}. Deux publications sont en cours de préparation en
vue de rendre public les travaux du chapitre \ref{chap:partie-multi-modele} et
du chapitre \ref{chap:partie-otb}.

\printbibliography[heading=subbibliography]

\subsection*{Autres contributions}
\begin{itemize}
  \item Encadrement à 50\% du stage de Master 1 de Romain Carriquiry Borchiari
    intitulé «Modélisation et simulation d'une population de bactéries en
    \opencl» duquel la toute première version de \otb est issu.
\end{itemize}

